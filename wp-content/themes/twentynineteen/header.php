<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<?php wp_head(); ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="js/theme.js"></script>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

		<header id="header" class="<?php echo is_singular() && twentynineteen_can_show_post_thumbnail() ? 'site-header featured-image' : 'site-header'; ?>">
			<div class="container-header">
				<div class="logo-header">
					<a href="/">
						<img src="/wspdemo-site/wp-content/uploads/2019/05/wsp-logo-1.png">
					</a>
				</div>
				<div class="header_mainmenu">
					<nav class="mainmenu_wrapper">
						<ul class="menu-main-menu">
							<li class="navPages-item">
								<a class="navPages-action" href="">Home</a>
							</li>
							<li class="navPages-item">
								<a class="navPages-action" href="">Job</a>
							</li>
							<li class="navPages-item">
								<a class="navPages-action" href="">Blog</a>
							</li>
							<li class="navPages-item">
								<a class="navPages-action" href="">Services</a>
								<div class="navPage-subMenu">
									<ul class="navPage-subMenu-list">
										<li class="sub-navPage-subMenu-list">
											<a href="">Open Source Framework</a>
											<div class="content-sub-navPage-subMenu-list">
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Magento</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Wordpress</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Yii</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Shopify</a>
													</li>
												</ul>

												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">uCommerce</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Akeneo</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Woocommerce</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Bigcommerce</a>
													</li>
												</ul>

												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Sharepoint</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Phalcon</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Sitecore</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Cake PHP</a>
													</li>
												</ul>

												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Laravel</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Joomla</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Umbraco</a>
													</li>
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Drupal</a>
													</li>
												</ul>
											</div>
										</li>

										<li class="sub-navPage-subMenu-list">
											<a href="">Mobile App</a>
											<div class="content-sub-navPage-subMenu-list">
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">IOS</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Android</a>
													</li>
												</ul>
											</div>
										</li>

										<li class="sub-navPage-subMenu-list">
											<a href="">Web Application</a>
											<div class="content-sub-navPage-subMenu-list">
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">HRM System</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">CRM System</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Payroll System</a>
													</li>
												</ul>
											</div>
										</li>

										<li class="sub-navPage-subMenu-list">
											<a href="">Services</a>
											<div class="content-sub-navPage-subMenu-list">
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Website Optimization</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Seo Services</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Graphic Design</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Devops</a>
													</li>
												</ul>
											</div>
										</li>

										<li class="sub-navPage-subMenu-list">
											<a href="">API Integration Service</a>
											<div class="content-sub-navPage-subMenu-list">
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Okta</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">API Third Party</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Magento - 2C2P</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Magento - Netsuite</a>
													</li>
												</ul>
											</div>
										</li>

										<li class="sub-navPage-subMenu-list">
											<a href="">Technology</a>
											<div class="content-sub-navPage-subMenu-list">
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">JavaScript</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">CodeIgniter</a>
													</li>
												</ul>
												<ul class="navPage-childList">
													<li class="sub-navPage-childList">
														<img src="">
														<a href="">Symfony</a>
													</li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
							<li class="navPages-item">
								<a class="navPages-action" href="">Our projects</a>
							</li>
							<li class="navPages-item">
								<a class="navPages-action" href="">Our team</a>
							</li>
							<li class="navPages-item">
								<a class="navPages-action" href="">Contact</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="header_right">
					<a href="tel:1-800-123-4567">1-800-123-4567</a>
				</div>
			</div>
		</header>

	<div id="content" class="site-content">

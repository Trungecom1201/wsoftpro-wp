
// hover menu

$('#header .header_mainmenu .content-sub-navPage-subMenu-list:first').addClass('active');
$('#header .header_mainmenu .sub-navPage-subMenu-list:first').addClass('active');


$('#header .header_mainmenu .sub-navPage-subMenu-list').hover(function () {

    const $this = $(this);
    const target = $this.data('collapsible');
    var childDivs = $('#header .header_mainmenu .content-sub-navPage-subMenu-list');

    for( var i=0; i< childDivs.length; i++ )
    {
        var a = $(childDivs[i]);
        var dataSubMenu = a.data('collapsible');
        if(dataSubMenu == target){
            $('#header .header_mainmenu .sub-navPage-subMenu-list').removeClass('active');
            $this.addClass('active');
            childDivs.removeClass('active');
            a.addClass('active');
        }
    }

});


if ($(window).width() > 992) {
    var lastScrollTop = 0;
    $(window).scroll(function(event){
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
            $('#header .container-header').css('height', '93px');
            $('#header .search_modal_button').hide();
            $('.site-header').addClass('header-active');
        } else {
            $('#header .container-header').css('height', '129px');
            $('#header .search_modal_button').show();
            $('.site-header').removeClass('header-active');
        }
    });
}
else {
    var lastScrollTop = 0;
    $(window).scroll(function(event){
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
            $('.site-header').addClass('header-active');
        } else {
            $('.site-header').removeClass('header-active');
        }
    });

}

$(window).resize(function() {

});



$('.scroll-wrapper .nav-menu-scroll>i').click(function(){
    $(this).next().slideToggle();
    $(this).toggleClass('fa-minus');
    $(this).toggleClass('fa-plus');
});

$('.scroll-wrapper .nav-item-sub>i').click(function(){
    $(this).next().slideToggle();
    $(this).toggleClass('fa-minus');
    $(this).toggleClass('fa-plus');
});


$('#header .header_right .toggle_menu_side').click(function(){
   $('#header .scroll-wrapper').toggleClass('scroll-wrapper-active');
});


$('#header .toggle-menu-mobile').click(function(){
    $('#header .close-menu-mobile').show();
    $('#header .header_mainmenu').addClass('header_mainmenu_active');
});

$('#header .close-menu-mobile .close-menu-mobile-nav').click(function(){
    $(this).parent().hide();
    $('#header .header_mainmenu').removeClass('header_mainmenu_active');
});
$(document).ready(function () {
    $('#carousel').flexslider({
        controlNav: false,
        animation: "slide",
        animationLoop: false,
        direction: "vertical",
        slideshow: false,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "fade",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
    $("#carousel li").click(function () {
        $("#carousel li").removeClass("slide-active");
        $(this).addClass('slide-active');
    })
});